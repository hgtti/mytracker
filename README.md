
[MyTracker](https://bitbucket.org/myuplay/mytracker)
====================================================

A free and open source torrent tracker written for the google appengine.

This software runs for free on the google appengine using minimal resources and keeps maintains responce times.

Usage
-----

Add `http://tracker.hgtti.com/announce` to your trackers.


Disclaimer
----------

We cannot/do not have the capacity to take down specific hashes, ban torrents, or do any other sorts of filtering.
We have the ability to blacklist ips connecting to the tracker but that is all. Unless you want to pay for a premium instance of the google appengine for the added queries to the datastore and cpu time then it won't be possible to do anything more than that.

We can respond to DMCA requests for things that are actually served from our servers. This only includes this website and blacklisting clients. That is currently best effort as we do not have the funds to do anything additional. (Its designed to be free)

All DMCA or related complaints should be taken up on our [issue page](https://bitbucket.org/myuplay/mytracker/issues?status=new&status=open).

Privacy
-------

No specific information is recorded about any of the peers connecting to this tracker beyond standard tracker information. There is no easy way to lookup torrents or peers beyond standard tracker usage. It takes only 30 minutes for a peer to expire in our memory and our logs don't hold information that is useful to anyone but developers.

In the event you wish to have the logs cleared for some specific purpose, please open a request on our [issue page](https://bitbucket.org/myuplay/mytracker/issues?status=new&status=open). We will get to it as soon as I check my email (varies).

Features
--------
Currently the software is still in a beta status meaning that it likely still has flaws
that could be considered problematic but the system will still function.

* Implemented announce

Future features/In Progress
---------------------------

* Scrape
* Improved main page
* Compact Response

Features that will not be added (for now)
-----------------------------------------

__UDP:__ UDP is not currently possible with appengine (it is on the cloud but that is expensive). It also suffers
from the same problem as compact responses and until this is improved, I have no intentions of implementing it.

__Numwant reponses greater than 30:__ This has been set as a soft limit in the design of the tracker. All responses will
only reply with up to 30 peers. We have done this because statistically you should never need more to increase speed AND
it saves us a ton of bandwidth when torrents reach the many thousands of peers mark. I have no intention of changing this
for the time being, however I may improve the quality of the 30 peers you recieve; for instance, if you are a seeder then
it makes sense to only give you leechers and leechers should recieve a mix of both.

__Key:__ We have not implemented the key entry for urls. It is currently just ignored. It may be implemented in the future.

