package MyTracker

import (
	"appengine"
	"appengine/memcache"
	"bytes"
	uuid "code.google.com/p/go-uuid/uuid"
	"encoding/gob"
	"encoding/hex"
	"encoding/json"
	bencode "github.com/jackpal/bencode-go"
	"math/rand"
	//"net"
	"net/http"
	"strconv"
	"time"
)

//Because the math package is stupid...
func min(a, b int) int {
	if a > b {
		return b
	} else {
		return a
	}
}

//Generics aren't that scary. They have a purpose.
func max(a, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

//Format for storing in memcache (better types than bencoded)
//Note: Uppercase to export fields.
type Client struct {
	Port uint16
	Ip   string
	Id   string
}

//Format for sending in bencoding. (Forces correct types (usually...))
type Peer map[string]interface{}

func makePeer(client Client) Peer {
	peer := make(Peer)
	peer["peer id"] = client.Id
	peer["ip"] = client.Ip
	peer["port"] = client.Port
	return peer
}

type Torrent struct {
	//Hash is implicitly known in the memcache structure as it is the key.
	Complete   uint
	Incomplete uint
	Peers      []string //An array of trackerids that correlate to client entries.
}

func (t *Torrent) add(peer string) {
	if t.Peers == nil {
		t.Peers = make([]string, 1)
	}

	t.Peers = append(t.Peers, peer)
}

func (t *Torrent) remove(id string) {
	for index, element := range t.Peers {
		if element == id {
			//Delete the element.
			t.Peers = append(t.Peers[:index], t.Peers[index+1:]...)
			return
		}
	}
}

func init() {
	http.HandleFunc("/announce", announce)
	//Eventually we will handle /scrape and maybe /stats
}

func fail(w http.ResponseWriter, c appengine.Context, message string) {
	var err map[string]string
	err = make(map[string]string)
	err["failure reason"] = message
	bencode.Marshal(w, err)
	c.Infof(message)
}

func debug(context appengine.Context, data interface{}, s string) {
	out, _ := json.Marshal(data)
	if len(s) == 0 {
		s = "Debug:"
	}
	context.Debugf("%s %v", s, string(out))
}

func announce(w http.ResponseWriter, r *http.Request) {
	context := appengine.NewContext(r)

	//Strings representing the torrent hash and the peer id.
	var hash, peerId string
	//Port that the client is listening on for torrents.
	var port uint16

	if val := r.FormValue("info_hash"); len(val) != 20 {
		fail(w, context, "Missing info_hash parameter")
		return
	} else { //We only want the hex version of the hash, not binary.
		hash = hex.EncodeToString([]byte(val))
	}

	if peerId = r.FormValue("peer_id"); len(peerId) == 0 || len(peerId) > 20 {
		fail(w, context, "Missing peer_id parameter")
		return
	}

	//We need the listening port from the peer
	if val, err := strconv.Atoi(r.FormValue("port")); err != nil {
		fail(w, context, "Missing port parameter")
		return
	} else {
		port = uint16(val)
	}

	//Ip to use to connect to the client, either their address they are
	//connecting with or the one they provide.
	var ip string
	if val := r.FormValue("ip"); len(val) > 0 {
		ip = val //ipv4, ipv6, and uri
	} else if val := r.FormValue("ipv6"); len(val) > 0 {
		ip = val //ipv6
	} else {
		ip = r.RemoteAddr
		/*
			if val, _, err := net.SplitHostPort(r.RemoteAddr); err == nil {
				context.Debugf("RemoteAddr: %v", r.RemoteAddr)
				context.Debugf("Got ip: %v", val)
				ip = val
			} else {
				context.Errorf("Failed to read the ip! %v", err)
				fail(w, "Internal error (failed to get ip)")
			}
		*/
	}

	var client = Client{
		Ip:   ip,
		Id:   peerId,
		Port: port,
	}

	//Unused...
	//uploaded := r.FormValue("uploaded")
	//downloaded := r.FormValue("downloaded")
	//left := r.FormValue("left")
	trackerId := r.FormValue("trackerid")

	//If the client is doing something interesting like stopping it will happen here.
	switch r.FormValue("event") {
	case "started":
		trackerId = uuid.New()
		if err := addPeer(context, hash, client); err != nil {
			fail(w, context, "Failed to add peer: "+err.Error())
			return
		}
		break
	case "stopped":
		go removePeer(context, hash, client.Id)
		break
	case "completed":
		if val := completed(context, hash); val != nil {
			fail(w, context, val.Error())
			return
		}
	default: //If the torrent is either completed or just doing a standard update, touch its entry.
		/* We are no longer tracking via trackerId as it is unreliable.
		if len(trackerId) == 0 {
			fail(w, context, "Missing trackerid! (Your client may be incomplete)")
			debug(context, client, "Client was missing trackerId:")
			return
		}*/
		go touch(context, client)
	}

	var peers []Peer
	if val, err := getPeers(context, hash, client.Id); err != nil {
		fail(w, context, "Torrent does not exist")
		context.Warningf("Failed to get peers: %s", err.Error())
		return
	} else {
		peers = val
	}

	//Randomly filter out peers and only keep the results.
	var numwant int
	numwant = 30
	if numwants := r.FormValue("numwant"); len(numwants) > 0 {
		if val, err := strconv.Atoi(numwants); err == nil {
			numwant = min(max(val, 0), 30)
		} //Skip it if it fails. We don't care.
	}

	var count int
	size := len(peers)
	count = min(numwant, size)
	fpeers := make([]Peer, count) //We could have 0 peers.
	index := 0
	if size > 0 { //Random panics if the size is <= 0
		index = rand.Intn(size) //We instead just use a random start index.
	}
	for i := 0; i < count; i++ {
		fpeers[i] = peers[(index+i)%size] //Loop cut.
	}

	var response map[string]interface{}
	response = make(map[string]interface{})

	response["interval"] = 60 * 15    //15 mins.
	response["min interval"] = 60 * 5 //5 mins.
	response["tracker id"] = trackerId
	response["peers"] = fpeers //Binary model? (No, doesn't support ipv6/dns)

	bencode.Marshal(w, response)
	debug(context, response, "Response: ")

	context.Infof("Request for %v resulted in %v peers to be returned.", hash, len(fpeers))

}

/**
Memcache structure:

Torrent: { key: T + hash, value: { {Torrent struct} } }
Client: { key: P + peerId, value { {Client struct} } }

Old clients will be marked using a C + trackerId, this is outdated and was replaced with the peerId

Memcache rules:
Always read and write quickly to minimize overwriting with old data. Most operations
will use CompareAndSwap to resist clobering data.

*/

func addPeer(context appengine.Context, hash string, client Client) error {
	var t Torrent

	item := &memcache.Item{
		Key:        "P" + client.Id,
		Object:     client,
		Expiration: time.Duration(30) * time.Minute, //30m expiration
	}

	if err := memcache.Gob.Set(context, item); err != nil {
		context.Errorf("Failed to set a value for P%s", client.Id)
		return err
	}

	if item, err := memcache.Gob.Get(context, "T"+hash, &t); err == nil {
		t.add(client.Id) //Add to the old tracker
		t.Incomplete++
		item.Object = t
		item.Expiration = time.Duration(60) * time.Minute //60m expiration.
		if err := memcache.Gob.CompareAndSwap(context, item); err != nil {
			context.Errorf("Failed to add peer to T%s", hash)
			return err
		}
	} else { //The entry doesn't exist.
		t.add(client.Id) //Add to a new tracker
		t.Incomplete = 1
		item := &memcache.Item{
			Key:        "T" + hash,
			Object:     t,
			Expiration: time.Duration(60) * time.Minute, //60m expiration.
		}
		if err := memcache.Gob.Set(context, item); err != nil {
			context.Errorf("Failed to create torrent T%s", hash)
			return err
		}
	}

	return nil //Success
}

func removePeer(context appengine.Context, hash string, id string) {

	var t Torrent

	memcache.Delete(context, "P"+id)

	if item, err := memcache.Gob.Get(context, "T"+hash, &t); err == nil {
		t.remove(id)
		item.Object = t
		item.Expiration = time.Duration(60) * time.Minute //60m expiration.
		memcache.Gob.CompareAndSwap(context, item)        //We don't mind errors here, just fail and keep going.
	} // We can ignore failures to remove it from the torrent, they will be fixed later.

}

//Increments completed value on torrent and decrements incomplete value.
func completed(context appengine.Context, hash string) error {
	var tor Torrent
	if item, err := memcache.Gob.Get(context, "T"+hash, tor); err == nil {
		tor.Complete++
		tor.Incomplete--
		item.Object = tor
		//debug(context, tor, "Torrent contruct as of completion:")
		if err := memcache.Gob.CompareAndSwap(context, item); err != nil {
			return err
		}
	} else {
		return err
	}
	return nil
}

//This function retrieves all of the peers listening on a torrent. It also ensures
//that the requesting client is in the list of active peers.
func getPeers(context appengine.Context, hash string, id string) ([]Peer, error) {

	var t Torrent

	if item, err := memcache.Gob.Get(context, "T"+hash, &t); err == nil {

		//This will always be at least the length of the peers but could add one.
		ids := make([]string, len(t.Peers), len(t.Peers)+1)

		seen := false
		for _, element := range t.Peers {
			if element == id {
				seen = true
			}
			ids = append(ids, "P"+element)
		}
		if seen == false {
			ids = append(ids, "P"+id) //The client should have been re-added by touch
			context.Debugf("Client (%s) should have been readded by touch", id)
		}

		peerMap, _ := memcache.GetMulti(context, ids)

		peers := make([]Peer, 0, len(ids))   //We could have 0 peers, max len(ids)
		valid := make([]string, 0, len(ids)) //Still valid Clients

		debug(context, peerMap, "PeerMap:")
		for key, value := range peerMap {
			//Parse all of the peers into Client structs and then use makePeer
			var client Client
			var buf bytes.Buffer

			buf.Write(value.Value)

			debug(context, value, "value:")

			dec := gob.NewDecoder(&buf)

			if err := dec.Decode(&client); err != nil {
				context.Warningf("Failed to decode an entry: %v", err)
				continue //Skip this entry, it seems invalid anyways.
			}

			peer := makePeer(client)

			peers = append(peers, peer)
			valid = append(valid, key[1:]) //Skip first letter, we know what type it is.
		}

		go func() {
			//Reinput the torrent key with only the valid clients.
			t.Peers = valid

			item.Object = t
			item.Expiration = time.Duration(60) * time.Minute //60m expiration
			if err := memcache.Gob.CompareAndSwap(context, item); err != nil {
				context.Warningf("Concurrency failure on updating usable clients in %v", hash)
			}
		}() //Concurrent call. This doesn't need to wait.

		return peers, nil

	} else {
		return nil, err
	}

	//This should not be reached.
}

//This will touch the client to ensure that it stays alive in the cache.
//If the client doesn't exist due to expiration then recreate it. It will be added to the torrent in the getPeers.
func touch(context appengine.Context, client Client) {

	if item, err := memcache.Get(context, "P"+client.Id); err == nil { //We don't even need to decode the object.
		item.Expiration = time.Duration(30) * time.Minute
		memcache.Set(context, item) //We don't care about overwrites because the data should never change, only the expiration
	} else {
		context.Infof("Client (%s) expired. Reinstating.", client.Id)
		item := &memcache.Item{
			Key:        "P" + client.Id,
			Object:     client,
			Expiration: time.Duration(30) * time.Minute,
		}
		memcache.Gob.Set(context, item)
	}
}
